<?php
namespace Pesquisa;

require_once('./bootstrap.php');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//$dbc = true;
$dbc = new \Connection(PDO_DRIVER, PDO_DATABASE, PDO_HOST, PDO_USER, PDO_PASSWORD);

$container = new \Slim\Container;
$container['view'] = function ($container) {
	$view = new \Slim\Views\Twig(VIEWS_PATH, [
		//'cache' => CACHE_PATH
	]);

	$view->addExtension(new \Slim\Views\TwigExtension(
		$container['router'],
		$container['request']->getUri()
	));

	return $view;
};

$container['dbc'] = $dbc;

$app = new \Slim\App($container);
$container = $app->getContainer();

$app->group('/api', function () {
	$this->get('/cursos[/]', 'Pesquisa\Controllers\ApiCursos:todosItems');
	$this->post('/cursos/nova[/]', 'Pesquisa\Controllers\ApiCursos:novo');
	$this->post('/cursos/editar/{id}[/]', 'Pesquisa\Controllers\ApiCursos:editar');
	$this->get('/cursos/deletar/{id}[/]', 'Pesquisa\Controllers\ApiCursos:deletar');
	$this->get('/cursos/{id}[/]', 'Pesquisa\Controllers\ApiCursos:buscaPorId');

	$this->get('/pesquisas[/]', 'Pesquisa\Controllers\ApiPesquisas:todosItems');
	$this->post('/pesquisas/nova[/]', 'Pesquisa\Controllers\ApiPesquisas:novo');
	$this->post('/pesquisas/editar/{id}[/]', 'Pesquisa\Controllers\ApiPesquisas:editar');
	$this->get('/pesquisas/deletar/{id}[/]', 'Pesquisa\Controllers\ApiPesquisas:deletar');
	$this->get('/pesquisas/{id}[/]', 'Pesquisa\Controllers\ApiPesquisas:buscaPorId');
});

$app->get('/', 'Pesquisa\Controllers\Pagina:cadastroPesquisa');
$app->get('/lista[/]', 'Pesquisa\Controllers\Pagina:listarPesquisas');

$app->run();

?>
