CREATE DATABASE IF NOT EXISTS `pesquisa` CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pesquisa`;

/**
 * Tabelas Independentes
 * 
 */

CREATE TABLE IF NOT EXISTS `cursos` (
	`id`      			int(4)     		NOT NULL,
	`nome`      		varchar(255),
	PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

/**
 * Tabelas de Usuários
 * 
 */

CREATE TABLE IF NOT EXISTS `pesquisas` (
	`id`      			int(4)     		NOT NULL,
	`nome`    			varchar(255),
	`sobrenome`    		varchar(255),
	`sexo`    			varchar(255),
	`data_nascimento`   varchar(255),
	`cidade`    		varchar(255),
	`celular`    		varchar(255),
	`email`    			varchar(255),
	`interesses`    	varchar(255),
	PRIMARY KEY (id)
) DEFAULT CHARSET=utf8;

INSERT INTO `cursos` VALUES(1001, 'Técnico em Manutenção Automotiva');
INSERT INTO `cursos` VALUES(1002, 'Técnico em Administração');
INSERT INTO `cursos` VALUES(1003, 'Técnico em Logística');
INSERT INTO `cursos` VALUES(1004, 'Técnico em Informática');
INSERT INTO `cursos` VALUES(1005, 'Técnico em Manutenção e Suporte em Informática');
INSERT INTO `cursos` VALUES(1006, 'Mecânico de Automóveis Leves');
INSERT INTO `cursos` VALUES(1007, 'Reparador de Sistemas de Injeção Eletrônica');
INSERT INTO `cursos` VALUES(1008, 'Mecânico de Manutenção de Motocicletas');
INSERT INTO `cursos` VALUES(1009, 'Pintor de Automóveis');
INSERT INTO `cursos` VALUES(1010, 'Técnicas de Pintura, Restauração, Polimento e Espelhamento Automotivo');
INSERT INTO `cursos` VALUES(1011, 'Soldador');
INSERT INTO `cursos` VALUES(1012, 'Eletricista de Automóveis');
INSERT INTO `cursos` VALUES(1013, 'Operação de Empilhadeira (NR-11)');
INSERT INTO `cursos` VALUES(1014, 'Atualização em Operação de Empilhadeira (NR-11)');
INSERT INTO `cursos` VALUES(1015, 'Operador de Processos de Produção');
INSERT INTO `cursos` VALUES(1016, 'Treinamento de Segurança para Operadores de Caldeira (NR-13)');
INSERT INTO `cursos` VALUES(1017, 'Diagnóstico e Manutenção de Sistemas de Transmissão Automática');
INSERT INTO `cursos` VALUES(1018, 'Transmissão Duallógic');
INSERT INTO `cursos` VALUES(1019, 'Técnicas de Polimento e Espelhamento Automotivo');
INSERT INTO `cursos` VALUES(1020, 'Pintor Preparador Automotivo');
INSERT INTO `cursos` VALUES(1021, 'Eletricidade Automotiva');
INSERT INTO `cursos` VALUES(1022, 'Alimhamento e Balanceamento de Rodas');
INSERT INTO `cursos` VALUES(1023, 'Excel Básico');
INSERT INTO `cursos` VALUES(1024, 'Excel Intermediário');
INSERT INTO `cursos` VALUES(1025, 'Excel Avançado');
INSERT INTO `cursos` VALUES(1026, 'AutoCad Básico');
INSERT INTO `cursos` VALUES(1027, 'Webdesign - Desenvolvimento de Website com HTML5 e CSS3 (Design Responsivo)');
INSERT INTO `cursos` VALUES(1028, 'Técnicas de Comunicação com o Cliente');
INSERT INTO `cursos` VALUES(1029, 'Fundamentos de Logística');
INSERT INTO `cursos` VALUES(1030, 'Fundamentos para Gestão de Estoque');
INSERT INTO `cursos` VALUES(1031, 'Assistente de Planejamento e Controle de Produção');
INSERT INTO `cursos` VALUES(1032, 'Assistente de Controle de Qualidade');