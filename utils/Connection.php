<?php
/**
* Connection
*/
class Connection extends \PDO
{
	
	function __construct($driver, $db, $host, $user, $pwd)
	{
		$dsn = $driver . ':dbname=' . $db . ';host=' . $host;
		parent::__construct($dsn, $user, $pwd);
	}

}

?>