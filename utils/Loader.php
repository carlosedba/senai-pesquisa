<?php
require_once(UTILS_PATH . 'Psr4AutoloaderClass.php');
require_once(UTILS_PATH . 'Pathfinder.php');

/**
* Loader
*/
class Loader
{
	private $loader = NULL;
	private $directoriesLength = 0;
	private $directoriesToSearch = array();
	private $directoriesFound = array();
	private $prefixes = array();
	private $namespaces = array();
	private $namespacesLength = 0;
	
	public function __construct()
	{
		$this->loader = new \Psr4AutoloaderClass;
		$this->loader->register();
	}

	public function addNamespace($namespace, $path)
	{
		//echo 'Namespace: ' . $namespace . ' - Caminho: ' . $path . '<br>';
		$this->loader->addNamespace($namespace, $path);
	}

	public function autoAddNamespace($directories = array(), $prefixes = array())
	{
		$this->directoriesLength = count($directories);
		$this->directoriesToSearch = $directories;
		$this->prefixes = $prefixes;
		$this->searchIntoDirectories();
		$this->getNamespaces();
		
		for ($i=0; $i < $this->directoriesLength; $i++) { 
			$namespacesLength = count($this->namespaces[$i]);
			for ($j=0; $j < $namespacesLength; $j++) {
				//echo 'Namespace: ' . $this->namespaces[$i][$j] . ' - Caminho: ' . $this->directoriesFound[$i][$j] . '<br>';
				$this->loader->addNamespace($this->namespaces[$i][$j], $this->directoriesFound[$i][$j]);
			}
		}
	}

	private function searchIntoDirectories()
	{
		foreach ($this->directoriesToSearch as $directory) {
			$pathfinder =  new \Pathfinder($directory);
			foreach ($pathfinder as $paths) {
				array_push($this->directoriesFound, $paths);
			}
		}

		//$this->directoriesFound = array_unique($this->directoriesFound);
		//var_dump($this->directoriesFound);
	}

	private function getNamespaces()
	{

		for ($i=0; $i < $this->directoriesLength; $i++) {
			$search = $this->directoriesToSearch[$i];
			$search = (substr($search, -1) === '\\') ? $search : $search . '\\';
			$replace = '';

;			$namespace = str_replace($search, $replace, $this->directoriesFound[$i]);
			array_push($this->namespaces, $namespace);
		}

		if (count($this->prefixes)) {
			for ($i=0; $i < $this->directoriesLength; $i++) { 
				if (isset($this->prefixes[$i])) {
					$prefixes = $this->prefixes[$i] . '\\';
					$prefixesLength = strlen($prefixes);
					$namespaceLength = count($this->namespaces[$i]);

					for ($j=0; $j < $namespaceLength; $j++) { 
						$strlen = strlen($this->namespaces[$i][$j]);
						$this->namespaces[$i][$j] = str_pad($this->namespaces[$i][$j], ($strlen + $prefixesLength), $prefixes, STR_PAD_LEFT);
					}
				}
			}
		}

		//var_dump($this->namespaces);
	}
}
?>
