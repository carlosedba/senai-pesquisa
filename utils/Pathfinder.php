<?php

/**
* Pathfinder
*/
class Pathfinder
{
	public $container = array();
	
	public function __construct($path)
	{
		$this->directoryIterator($path);
		$this->directoryCleaner();
		
		return $this->container;
	}

	public function forEachDirectory(callable $callback = NULL)
	{
		foreach ($this->container as $dir) {
			if ($callback) {
				call_user_func($callback, $dir);
			}
		}
	}

	private function directoryCleaner()
	{
		$containerLength = count($this->container);

		for ($i=0; $i <$containerLength; $i++) {
			if (isset($this->container[$i])) {
				if (is_array($this->container[$i])) {
					$this->container = array_merge($this->container, $this->container[$i]);
					unset($this->container[$i]);
				}
			}
		}

		for ($i=0; $i <$containerLength; $i++) {
			if (isset($this->container[$i])) {
				if (is_array($this->container[$i])) {
					unset($this->container[$i]);
				}
			}
		}

		$this->container = array_unique($this->container);
	}
	
	private function directoryIterator($path)
	{
		$iterator = new DirectoryIterator($path);
		$status = NULL;

		foreach ($iterator as $iteration) {
			$dir = $iteration->getPath() . "\\" . $iteration->getFilename();

			if (!$iteration->isDot()) {
				if ($iteration->isDir()) {
					array_push($this->container, $dir);

					if ($iteration->valid()) {
						$this->directoryIterator($dir);
					}
				}
			}
		}

		$status = (count($this->container)) ? true : false;
		return $status;
	}
}

?>