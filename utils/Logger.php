<?php
/**
* Logger
*/
class Logger
{

	private $settings;
	
	public function __construct(array $userSettings = array())
	{
		$this->settings = array_merge($this->getDefaultSettings(), $userSettings);
	}

	private function getDefaultSettings()
	{
		return array(
			"setErrorHandler" => true
			);
	}

	public function setErrorHandler()
	{
		if ($this->settings['setErrorHandler']) {
			set_error_handler(array($this, 'errorHandler'));
			register_shutdown_function(array($this, 'fatalErrorHandler'));
		}
	}

	public function devLog($tag, $msg, $file = NULL, $line = NULL)
	{
	    $date = date("d-m-Y");
	    $time = date("H:i:s");
	    $timestamp = date("T");

	    $logFile = LOG_PATH . 'dev.log';

	    $log = "[" . $tag . "][" . $date . "][" . $time . "]" . "[" . $msg . "]\n";
	    if ($file !== NULL) {
	    	$log .= "[FILE][" . $file . "]\n";
	    }
	    if ($line !== NULL) {
	    	$log .= "[LINE][" . $line . "]\n";
	    }
	    $log .= "\n";

	    file_put_contents($logFile, $log, FILE_APPEND);
	}

	public function errorHandler($errno, $errmsg, $filename, $linenum, $vars = NULL) 
	{
	    // timestamp for the error entry
	    $date = date("d-m-Y");
	    $time = date("H:i:s");
	    $timestamp = date("T");

	    // define an assoc array of error string
	    // in reality the only entries we should
	    // consider are E_WARNING, E_NOTICE, E_USER_ERROR,
	    // E_USER_WARNING and E_USER_NOTICE
	    $errortype = array (
	                E_ERROR              => 'FATAL ERROR',
	                E_WARNING            => 'WARNING',
	                E_PARSE              => 'PARSE ERROR',
	                E_NOTICE             => 'NOTICE',
	                E_CORE_ERROR         => 'CORE ERROR',
	                E_CORE_WARNING       => 'CORE WARNING',
	                E_COMPILE_ERROR      => 'COMPILE ERROR',
	                E_COMPILE_WARNING    => 'COMPILE WARNING',
	                E_USER_ERROR         => 'USER ERROR',
	                E_USER_WARNING       => 'USER WARNING',
	                E_USER_NOTICE        => 'USER NOTICE',
	                E_STRICT             => 'RUNTIME NOTICE',
	                E_RECOVERABLE_ERROR  => 'CATCHABLE FATAL ERROR'
	                );
	    // set of errors for which a var trace will be saved
	    $user_errors = array(E_USER_ERROR, E_USER_WARNING, E_USER_NOTICE);

	    $err = "[" . $errortype[$errno] . "][CODE " . $errno . "][" . $date . "][" . $time . "][" . $timestamp . "]\n";
	    $err .= "[MESSAGE][" . htmlspecialchars_decode($errmsg) . "]\n";
	    $err .= "[FILE][" . $filename . "]\n";
	    $err .= "[LINE][" . $linenum . "]\n";

	    if (in_array($errno, $user_errors)) {
	        $err .= "\t" . wddx_serialize_value($vars, "[VARIABLES]") . "\n";
	    }
	    $err .= "\n";

	    // save to the error log, and e-mail me if there is a critical user error
	    error_log($err, 3, LOG_PATH . 'error.log');
	    if ($errno == E_USER_ERROR) {
	        //mail("carlosedbalmeida@gmail.com", "[NP-LOGGER] - Critical User Error", $err);
	    }
	}

	public function fatalErrorHandler() 
	{
	    $error = error_get_last();

	    /*
		$this->devLog('FATAL_ERROR', $error);

	    foreach (array_keys($error) as $key) {
	    	$this->devLog('FATAL_ERROR', $key);
	    }
	    foreach ($error as $value) {
	    	$this->devLog('FATAL_ERROR', $value);
	    }
	    */

	    if ($error['type'] === E_ERROR) {
	    	$this::errorHandler($error['type'], $error['message'], $error['file'], $error['line']);
	    }
	}

	public function socketsErrorHandler($err)
	{
		// maybe no futuro...
	}
}

?>
