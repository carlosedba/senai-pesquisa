<?php
require_once('./headers.php');
require_once(UTILS_PATH . 'Loader.php');
require_once(UTILS_PATH . 'Connection.php');
require_once(UTILS_PATH . 'IdGenerator.php');
require_once(VENDOR_PATH . 'FastRoute\bootstrap.php');
require_once(VENDOR_PATH . 'Twig\Autoloader.php');
\Twig_Autoloader::register();
$loader = new \Loader();
$loader->autoAddNamespace(array(
	VENDOR_PATH
));
$loader->addNamespace('Pesquisa\Controllers', CONTROLLERS_PATH);
$loader->addNamespace('Pesquisa\Models', MODELS_PATH);

?>
