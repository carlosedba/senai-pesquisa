<?php
namespace Pesquisa\Models;

/**
* Pesquisa
*/
class Pesquisa
{

	public $id;
	public $nome;
	public $sobrenome;
	public $sexo;
	public $data_nascimento;
	public $cidade;
	public $celular;
	public $email;
	public $interesses;
	
	function __construct($data)
	{
		$this->id = \IdGenerator::uniqueNumberID();
		$this->nome = $data['nome'];
		$this->sobrenome = $data['sobrenome'];
		$this->sexo = $data['sexo'];
		$this->data_nascimento = $data['data_nascimento'];
		$this->cidade = $data['cidade'];
		$this->celular = $data['celular'];
		$this->email = $data['email'];
		$this->interesses = $data['interesses'];
	}

	static public function todosItens($Connection)
	{
		$query = $Connection->prepare("SELECT * FROM `pesquisas`");
		
		if($query->execute()) {
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}

	static public function buscaMultipla($Connection, $attribute, $value)
	{
		$query = $Connection->prepare("SELECT * FROM `pesquisas` WHERE `" . $attribute . "` = ?");
		
		if($query->execute(array($value))) {
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}
	
	static public function buscaUnica($Connection, $attribute, $value)
	{
		$query = $Connection->prepare("SELECT * FROM `pesquisas` WHERE `" . $attribute . "` = ?");

		if($query->execute(array($value))) {
			while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
				return $row;
			}
		} else {
			return false;
		}
	}
	
	static public function buscaPorId($Connection, $id)
	{
		$query = $Connection->prepare("SELECT * FROM `pesquisas` WHERE id = ?");
		
		if($query->execute(array(
			$id
		))) {
			while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
				return $row;
			}
		} else {
			return false;
		}
	}
	
	static public function criar($Connection, $Pesquisa)
	{
		$query = $Connection->prepare("INSERT INTO `pesquisas` (id, nome, sobrenome, sexo, data_nascimento, cidade, celular, email, interesses) values(?, ?, ?, ?, ?, ?, ?, ?, ?)");
		if($query->execute(array_values((array)$Pesquisa))) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function atualizar($Connection, $Pesquisa, $id)
	{
		$query = $Connection->prepare("UPDATE `pesquisas` SET nome = ?, sobrenome = ?, sexo = ?, data_nascimento = ?, cidade = ?, celular = ?, email = ?, interesses = ? WHERE id = ?");
		if($query->execute(array(
			$Pesquisa->nome,
			$Pesquisa->sobrenome,
			$Pesquisa->sexo,
			$Pesquisa->data_nascimento,
			$Pesquisa->cidade,
			$Pesquisa->celular,
			$Pesquisa->email,
			$Pesquisa->interesses,
			$id
		))) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function remover($Connection, $id)
	{
		$query = $Connection->prepare("DELETE FROM `pesquisas` WHERE id = ?");
		if($query->execute(array(
			$id
		))) {
			return true;
		} else {
			return false;
		}
	}
}

?>