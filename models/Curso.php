<?php
namespace Pesquisa\Models;

/**
* Curso
*/
class Curso
{

	public $id;
	public $nome;
	
	function __construct($data)
	{
		$this->id = \IdGenerator::uniqueNumberID();
		$this->nome = $data['nome'];
	}

	static public function todosItens($Connection)
	{
		$query = $Connection->prepare("SELECT * FROM `cursos`");
		
		if($query->execute()) {
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}

	static public function buscaMultipla($Connection, $attribute, $value)
	{
		$query = $Connection->prepare("SELECT * FROM `cursos` WHERE `" . $attribute . "` = ?");
		
		if($query->execute(array($value))) {
			return $query->fetchAll(\PDO::FETCH_ASSOC);
		} else {
			return false;
		}
	}
	
	static public function buscaUnica($Connection, $attribute, $value)
	{
		$query = $Connection->prepare("SELECT * FROM `cursos` WHERE `" . $attribute . "` = ?");

		if($query->execute(array($value))) {
			while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
				return $row;
			}
		} else {
			return false;
		}
	}
	
	static public function buscaPorId($Connection, $id)
	{
		$query = $Connection->prepare("SELECT * FROM `cursos` WHERE id = ?");
		
		if($query->execute(array(
			$id
		))) {
			while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
				return $row;
			}
		} else {
			return false;
		}
	}
	
	static public function criar($Connection, $Curso)
	{
		$query = $Connection->prepare("INSERT INTO `cursos` (id, nome) values(?, ?)");
		if($query->execute(array_values((array)$Curso))) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function atualizar($Connection, $Curso, $id)
	{
		$query = $Connection->prepare("UPDATE `cursos` SET nome = ? WHERE id = ?");
		if($query->execute(array(
			$Curso->nome,
			$id
		))) {
			return true;
		} else {
			return false;
		}
	}
	
	static public function remover($Connection, $id)
	{
		$query = $Connection->prepare("DELETE FROM `cursos` WHERE id = ?");
		if($query->execute(array(
			$id
		))) {
			return true;
		} else {
			return false;
		}
	}
}

?>