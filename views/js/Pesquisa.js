var Pesquisa = window.Pesquisa || function (options) {
	this.innerWidth = window.innerWidth
	this.innerHeight = window.innerHeight

	this.state = {
		currentStep: 0,
		inputValidation: false
	}

	this.DOM = {
		tableRows: document.querySelectorAll('.the-table tbody tr'),
		modalTitle: document.querySelector('.modal .title'),
		inputName: document.querySelectorAll('.modal .input-name'),
		inputPreview: document.querySelectorAll('.modal .input-preview'),

		openModal: document.querySelectorAll('.open-modal'),
		closeModal: document.querySelectorAll('.close-modal'),
		modalOverlay: document.querySelector('.modal-overlay'),
		modal: document.querySelector('.modal'),

		inputName: document.querySelectorAll('.input .input-name'),
		inputPreview: document.querySelectorAll('.input .input-preview'),
		input: document.querySelectorAll('.input input'),

		submit: document.querySelector('.btn-submit'),
		fechar: document.querySelectorAll('.fa-times'),

		btnCrud: document.querySelectorAll('.btn-crud'),
		content: document.querySelectorAll('.content'),
		contentWrapper: document.querySelectorAll('.content .wrapper')
	}
}

Pesquisa.prototype.ajax = function (url, token, callback) {
	var request = new XMLHttpRequest()
	request.open('GET', url, true)
	if (token) request.setRequestHeader('Authorization', 'Bearer ' + token)
	request.onload = function () {
		if (this.status === 200 && this.readyState === 4) {
			var json = JSON.parse(this.responseText)
			callback(null, json)
		} else {
			callback(true, null)
		}
	}
	request.send()
}

Pesquisa.prototype.setModalControls = function (e) {
	;[].forEach.call(this.DOM.openModal, function (el, ind, arr) {
		el.addEventListener('click', Pesquisa.prototype.openModal.bind(this))
	}, this)

	;[].forEach.call(this.DOM.closeModal, function (el, ind, arr) {
		el.addEventListener('click', Pesquisa.prototype.closeModal.bind(this))
	}, this)

	this.DOM.modalOverlay.addEventListener('click', Pesquisa.prototype.closeModal.bind(this))
	window.addEventListener('resize', Pesquisa.prototype.updateModalPosition.bind(this))
}

Pesquisa.prototype.updateModalPosition = function (e) {
	this.innerWidth = window.innerWidth
	this.innerHeight = window.innerHeight

	this.DOM.modal.style.top = (this.innerHeight - this.DOM.modal.offsetHeight) / 2 + 'px'
	this.DOM.modal.style.left = (this.innerWidth -  this.DOM.modal.offsetWidth) / 2 + 'px'
}

Pesquisa.prototype.openModal = function (e) {
	var index = e.target.parentNode.dataset.index
	var id = this.DOM.tableRows[index].dataset.id
	Pesquisa.prototype.setModalData(id, this)

	this.DOM.modalOverlay.style.display = 'block'
	this.DOM.modal.style.display = 'flex'

	this.DOM.modal.style.top = (this.innerHeight - this.DOM.modal.offsetHeight) / 3 + 'px'
	this.DOM.modal.style.left = (this.innerWidth -  this.DOM.modal.offsetWidth) / 2 + 'px'

	setTimeout(function () {
		this.DOM.modalOverlay.style.backgroundColor = 'rgba(0, 0, 0, 0.5)'
		this.DOM.modal.style.opacity = '1'
	}.bind(this), 160)
}

Pesquisa.prototype.closeModal = function (e) {
	Pesquisa.prototype.clearModalData(this)

	this.DOM.modalOverlay.style.backgroundColor = 'rgba(0, 0, 0, 0)'
	this.DOM.modal.style.opacity = '0'
	window.location.hash = ''

	setTimeout(function () {
		this.DOM.modalOverlay.style.display = 'none'
		this.DOM.modal.style.display = 'none'
	}.bind(this), 160)
}

Pesquisa.prototype.setModalData = function (id, ambient) {
	var ajax = Pesquisa.prototype.ajax
	var interesses = document.querySelector('.interesses')

	ajax(window.location.origin + '/pesquisa/api/pesquisas/' + id, null, function (err, data) {
		ambient.DOM.modalTitle.innerHTML = data.id + ' - ' + data.nome

		;[].forEach.call(ambient.DOM.inputName, function (el, ind, arr) {
			var field = el.getAttribute('for')

			switch (field) {
				case 'interesses':
					var ids = data[field].split(',')
					ids.pop()
					ids.forEach(function (el, ind, arr) {
						var span = document.createElement('span')
						span.setAttribute('class', 'interesse')

						ajax(window.location.origin + '/pesquisa/api/cursos/' + el, null, function (err, data) {
							span.innerHTML = data.nome
						})
						interesses.appendChild(span)
					})
					break;
				default:
					ambient.DOM.inputPreview[ind].innerHTML = data[field]
					break;
			}
		})
	})
}

Pesquisa.prototype.clearModalData = function (ambient) {
	var interesses = document.querySelector('.interesses')

	;[].forEach.call(ambient.DOM.inputPreview, function (el, ind, arr) {
		el.innerHTML = ''
	}, this)

	while (interesses.firstChild) {
		interesses.removeChild(interesses.firstChild);
	}
}

Pesquisa.prototype.setInputs = function () {
	;[].forEach.call(this.DOM.fechar, function (el, ind, arr) {
		el.addEventListener('click', function (e) {
			confirm('Você realmente deseja deletar?')
		}.bind(this))
	}, this)

	;[].forEach.call(this.DOM.input, function (el, ind, arr) {
		el.oninvalid = function (e) {
			this.state.inputValidation = true
		}.bind(this)
	}, this)
}

Pesquisa.prototype.setForm = function (forms) {
	;[].forEach.call(forms, function (el, ind, arr) {
		this.DOM.submit.addEventListener('click', function (e) {
			el.submit()
		}.bind(this))
	}, this)
}

