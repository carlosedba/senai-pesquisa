<?php
namespace Pesquisa\Controllers;

use Pesquisa\Models\Pesquisa;
use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
* ApiPesquisas
*/
class ApiPesquisas
{

	protected $ci;
	protected $dbc;

	public function __construct(ContainerInterface $ci) {
		$this->ci = $ci;
		$this->dbc = $ci->get('dbc');
	}

	public function todosItems(Request $request, Response $response, $args)
	{
		$json = json_encode(Pesquisa::todosItens($this->dbc));
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function buscaPorId(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$json = json_encode(Pesquisa::buscaPorId($this->dbc, $id));
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function novo(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();

		$interesses = '';
		foreach ($data['interesses'] as $interesse) {
			$interesses = $interesses . $interesse . ',';
		}

		$data['interesses'] = $interesses;

		$Pesquisa = new Pesquisa($data);

		if (Pesquisa::criar($this->dbc, $Pesquisa)) {
			$response = $response->withRedirect('/pesquisa');
			//$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function editar(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$Pesquisa = new Pesquisa($data);

		if (Pesquisa::atualizar($this->dbc, $Pesquisa, $id)) {
			$response = $response->withRedirect('/pesquisa/lista');
			//$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function deletar(Request $request, Response $response, $args)
	{
		$id = $args['id'];

		if (Pesquisa::remover($this->dbc, $id)) {
			$response = $response->withRedirect('/pesquisa/lista');
			//$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}

?>
