<?php
namespace Pesquisa\Controllers;

use Pesquisa\Models\Curso;
use Pesquisa\Models\Pesquisa;
/**
* Bundle
*/
class Bundle
{

	static public function cadastroPesquisa($dbc)
	{
		$cursos = Curso::todosItens($dbc);

		return array(
			'cursos' => $cursos
		);
	}

	static public function listarPesquisas($dbc)
	{
		$pesquisas = Pesquisa::todosItens($dbc);
		$cursos = Curso::todosItens($dbc);

		return array(
			'pesquisas' => $pesquisas,
			'cursos' => $cursos
		);
	}
}

?>
