<?php
namespace Pesquisa\Controllers;

use Pesquisa\Models\Pesquisa;
use Pesquisa\Controllers\Bundle;
use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
* Pagina
*/
class Pagina
{

	protected $ci;
	protected $dbc;
	protected $view;

	public function __construct(ContainerInterface $ci) {
		$this->ci = $ci;
		$this->dbc = $ci->get('dbc');
		$this->view = $ci->get('view');
	}

	public function cadastroPesquisa(Request $request, Response $response, $args)
	{
		$data = array();
		$bundle = Bundle::cadastroPesquisa($this->dbc);

		return $this->view->render($response, 'cadastro-pesquisa.html', array_merge($data, $bundle));
	}

	public function listarPesquisas(Request $request, Response $response, $args)
	{
		$data = array();
		$bundle = Bundle::listarPesquisas($this->dbc);

		return $this->view->render($response, 'lista.html', array_merge($data, $bundle));
	}
}

?>
