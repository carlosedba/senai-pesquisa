<?php
namespace Pesquisa\Controllers;

use Pesquisa\Models\Curso;
use \Interop\Container\ContainerInterface;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

/**
* ApiCursos
*/
class ApiCursos
{

	protected $ci;
	protected $dbc;

	public function __construct(ContainerInterface $ci) {
		$this->ci = $ci;
		$this->dbc = $ci->get('dbc');
	}

	public function todosItems(Request $request, Response $response, $args)
	{
		$json = json_encode(Curso::todosItens($this->dbc));
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function buscaPorId(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$json = json_encode(Curso::buscaPorId($this->dbc, $id));
		$response->getBody()->write($json);
		$response = $response->withAddedHeader('Content-Type','application/json');

		return $response;
	}

	public function novo(Request $request, Response $response, $args)
	{
		$data = $request->getParsedBody();
		$Curso = new Curso($data);

		if (Curso::criar($this->dbc, $Curso)) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function editar(Request $request, Response $response, $args)
	{
		$id = $args['id'];
		$data = $request->getParsedBody();
		$Curso = new Curso($data);

		if (Curso::atualizar($this->dbc, $Curso, $id)) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}

	public function deletar(Request $request, Response $response, $args)
	{
		$id = $args['id'];

		if (Curso::remover($this->dbc, $id)) {
			$response = $response->withStatus(201);
		} else {
			$response = $response->withStatus(400);
		}

		return $response;
	}
}

?>
